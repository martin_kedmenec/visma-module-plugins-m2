<?php

namespace Unit1\Plugins\Plugin;

use Magento\Theme\Block\Html\Breadcrumbs;

class BeforeBreadcrumbsPlugin
{
    /**
     * @param Breadcrumbs $subject
     * @param $crumbName
     * @param $crumbInfo
     * @return array
     */
    public function beforeAddCrumb(Breadcrumbs $subject, $crumbName, $crumbInfo): array
    {
        $crumbInfo['label'] = $crumbInfo['label'] . '(!b)';

        return [$crumbName, $crumbInfo];
    }
}
