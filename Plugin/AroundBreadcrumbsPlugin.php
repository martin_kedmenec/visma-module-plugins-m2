<?php

namespace Unit1\Plugins\Plugin;

use Magento\Theme\Block\Html\Breadcrumbs;

class AroundBreadcrumbsPlugin
{
    /**
     * @param Breadcrumbs $subject
     * @param callable $proceed
     * @param $crumbName
     * @param $crumbInfo
     * @return void
     */
    public function aroundAddCrumb(Breadcrumbs $subject, callable $proceed, $crumbName, $crumbInfo): void
    {
        $crumbInfo['label'] = $crumbInfo['label'] . '(!a)';
        $proceed($crumbName, $crumbInfo);
    }
}
