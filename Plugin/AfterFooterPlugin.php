<?php

namespace Unit1\Plugins\Plugin;

use Magento\Theme\Block\Html\Footer;

class AfterFooterPlugin
{
    /**
     * @param Footer $subject
     * @param $result
     * @return string
     */
    public function afterGetCopyright(Footer $subject, $result): string
    {
        return 'Lolz';
    }
}
