<?php

namespace Unit1\Plugins\Plugin;

use Magento\Catalog\Model\Product;

class AfterPricePlugin
{
    /**
     * @param Product $subject
     * @param $result
     * @return float
     */
    public function afterGetPrice(Product $subject, $result): float
    {
        return $result * 23.7;
    }
}
